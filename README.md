Das Projekt enthält den vollständigen Sourcecode der Bachelorarbeit "Analyse, Entwurf und Implementierung einer Zuordnungsstrategie für OpenMP-Prozesse in planungsbasierten Cluster-Verwaltungssystemen"

pinning Ordner:
Das Programm analyze.c testet das Bindungsverhalten von der originalen libgomp Implementierung und gibt das Bindungsverhalten aus. Hier lässt sich das implizite Pinning einer For-Schleife beobachten, aber auch die nicht näher spezifizierte Abarbeitungsreihenfolge der pragmas parallel und parallel sections. 

Zu benutzende Umgebungsvariablewerte (anpassen insofern keine 4 Hardwarethreads vorhanden sind):

export OMP_PLACES="{0},{1},{2},{3}"

export OMP_PROC_BIND=true


libgomp Ordner:
Libgomp ist die gcc Implementierung von OpenMP. Für die prototypische Entwicklung eines Pinning-Mechanismus von Threads zu Prozessorkernen wurde die libgomp Implementierung angepasst und um die Funktionalität erweitert (siehe Bachelorarbeit).

Änderungen wurden an den Folgenden Stellen vorgenommen (Datei + Funktionen):

env.c: parse_places_var, parse_place_pinning, addChunk, addPlacePinning, createChunk, createPlacePinning

iter.c: gomp_iter_dynamic_next

libgomp.h: place_pinning, chunk, gomp_work_share, gomp_thread

Die Einrichtung einer Umgebung zur Kompilation von gcc mit libgomp ist mit Folgenden Schritten möglich:
1. git clone git://gcc.gnu.org/git/gcc.git              // Klonen des vollständigen gcc Projektes
2. git branch -a                                        // Anzeige der Projektbranches [optional]
3. git checkout remotes/origin/releases/gcc-13          // Auschecken der aktuellen Version
4. Kopieren des libgomp Dateien in den vollständigen Code von gcc
5. mkdir builddirectory                                 // Erstellen eines neuen Verzeichnisses für die Buildartefakte; sollte nicht auf der Projektebene von gcc erfolgen, sondern in einem getrennten Verzeichnis
6. ./../gcc/configure --prefix=$HOME/GCC-13 --enable-languages=c,c++ --disable-multilib (64-bit-only compiler)                                               // Konfigurationsbefehl im Buildverzeichnis (Achtung Pfad zum gcc Projekt anpassen wenn Buildverzeichnis woanders liegt; Installationspfad ebenfalls anpassen insofern gewünscht)
7. https://gcc.gnu.org/install/prerequisites.html besuchen und Buildabhängigkeiten installieren; fehlende Abhängigkeiten werden auch als Fehlermeldung ausgegeben
8. sudo make -j [Anzahl Prozessorkerne]                 // paralleles Kompilieren des Projektes 
9. sudo make install                                    // Installation von gcc 


examples Ordner:
Das Testprogramm example_program.c testet den Pinning-Mechanismus. Dazu muss das Programm mit der angepassten libgomp Implementierung kompiliert werden (Testprogramm liegt alternativ bei).

Zu benutzende Umgebungsvariablewerte (anpassen insofern keine 4 Hardwarethreads vorhanden sind):

export OMP_PLACES="{0},{1},{2},{3}"

export OMP_PROC_BIND=true

export OMP_SCHEDULE=static

export OMP_NUM_THREADS=4

export OMP_DISPLAY_ENV=true

export OMP_DISPLAY_AFFINITY=true

export OMP_AFFINITY_FORMAT="Affinity: %0.3L %.8n %A %i %.12H"