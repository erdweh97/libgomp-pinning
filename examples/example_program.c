#include <stdio.h>
#include <omp.h>

void work(int id)
{
    // execute fibonacci calculation for workload
    long t1 = 0, t2 = 1, nextTerm = 1;

    for (int i = 0; i <= 1000000; i++)
    {
        t1 = t2;
        t2 = nextTerm;
        nextTerm = t1 + t2;
    }

    printf("OMP-Thread %d at place %d executed section %d \n", omp_get_thread_num(), omp_get_place_num(), id);
}

int main()
{
    for (int i = 0; i < 2; i++)
    {
        printf("---------------------------------------------\n");
        printf("Start of Parallel Section %d\n", i + 1);
        printf("---------------------------------------------\n");

#pragma omp parallel sections
        {
#pragma omp section
            work(0);

#pragma omp section
            work(1);

#pragma omp section
            work(2);

#pragma omp section
            work(3);

#pragma omp section
            work(4);

#pragma omp section
            work(5);

#pragma omp section
            work(6);

#pragma omp section
            work(7);
        }

        printf("---------------------------------------------\n");
        printf("End of Parallel Section %d\n", i + 1);
        printf("---------------------------------------------\n");
        printf("\n");
    }

    return 0;
}