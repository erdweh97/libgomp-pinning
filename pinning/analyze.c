#include <stdio.h>
#include <omp.h>

int main()
{
    printf("---------------------------------------------\n");
    printf("Start of for loop\n");
    printf("---------------------------------------------\n");

#pragma omp parallel num_threads(4)
    {
 
/* analoges Beispiel mit order(reproducible: concurrent) anstatt mit scheduler(static) 
ist nicht möglich, da OpenMP API Aufrufe in einer Region mit einem order(concurrent) Konstrukt 
nicht möglich sind (siehe https://www.openmp.org/spec-html/5.1/openmpsu47.html).
Die Aufrufe omp_get_thread_num() und omp_get_place_num() können dann nicht
verwendet werden.*/
#pragma omp for schedule(static, 1)
        for (int i = 0; i < 8; i++)
        {
            printf("OMP-Thread %d at place %d executed loop index %d\n", omp_get_thread_num(), omp_get_place_num(), i);
        }
    }

    printf("---------------------------------------------\n");
    printf("End of for loop\n");
    printf("---------------------------------------------\n");

    printf("---------------------------------------------\n");
    printf("Start of parallel section\n");
    printf("---------------------------------------------\n");

// close and true are the same
#pragma omp parallel sections proc_bind(close) num_threads(4)
    {
#pragma omp section
        printf("OMP-Thread %d at place %d executed section index 0 \n", omp_get_thread_num(), omp_get_place_num());

#pragma omp section
        printf("OMP-Thread %d at place %d executed section index 1 \n", omp_get_thread_num(), omp_get_place_num());

#pragma omp section
        printf("OMP-Thread %d at place %d executed section index 2 \n", omp_get_thread_num(), omp_get_place_num());

#pragma omp section
        printf("OMP-Thread %d at place %d executed section index 3 \n", omp_get_thread_num(), omp_get_place_num());
    }

    printf("---------------------------------------------\n");
    printf("End of parallel section\n");
    printf("---------------------------------------------\n");

    printf("---------------------------------------------\n");
    printf("Start of parallel region\n");
    printf("---------------------------------------------\n");

    int i = 0;

#pragma omp parallel proc_bind(close) num_threads(4) shared(i)
    {
#pragma omp critical
        {
            printf("OMP-Thread %d at place %d executed parallel index %d \n", omp_get_thread_num(), omp_get_place_num(), i++);
        }
    }

    printf("---------------------------------------------\n");
    printf("End of parallel region\n");
    printf("---------------------------------------------\n");

    return 0;
}
